#####################################
## AWS Provide Declaration
#####################################

provider "aws" {
    access_key = "${var.AWS_ACCESS_KEY}"
    secret_key = "${var.AWS_SECRET_KEY}"
    region = "${var.AWS_REGION}"
    access_tokem = "${var.AWS_SESSION_TOKEN}
}
