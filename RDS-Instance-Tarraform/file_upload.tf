resource "aws_instance" "example" {
    ami = "ami-d001245"
    instance_type = "t2.micro"


#####################################
## File Provisioner
## The file provisioner is used to copy files or directories from the machine executing Terraform to the newly created resource. The file provisioner supports both ssh and winrm type connections.
## https://www.terraform.io/language/resources/provisioners/file
#####################################

provisioner "file" {
    source      = "/tmp/scipt.sh"
    destination = "/etc/script.sh"
 #####################################
 ## To Override the SSH default we can use connection 
 #####################################  
    connection {
        user = "${var.instance_username}"
        password = "${var.instance_password}"
    }
#####################################
## remote-exec Provisioner
## The remote-exec provisioner invokes a script on a remote resource after it is created. This can be used to run a configuration management tool, bootstrap into a cluster, etc. 
## https://www.terraform.io/language/resources/provisioners/remote-exec
#####################################

provisioner "remote-exec" {
    inline = [
      "chmod +x /etc/script.sh",
      "sudo /etc/script.sh",
    ]
  }
}
