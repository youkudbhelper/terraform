#####################################
## Stores the state as a given key in a given bucket on Amazon S3.
## https://www.terraform.io/language/settings/backends/s3

## This assumes we have a bucket created called mybucket. The Terraform state is written to the key path/to/my/key.
#####################################

terraform {
  backend "s3" {
    bucket = "mybucket"
    key    = "path/to/my/key"
    region = "us-east-1"
  }
}

