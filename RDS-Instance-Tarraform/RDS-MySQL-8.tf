#####################################
## Configure the AWS Provider
#####################################

#provider "aws" {
#  region = "ap-south-1"
#}

## Please go through this url "terraform data source" to understand how data sources work
## https://www.terraform.io/language/data-sources

data "aws_availability_zones" "available" {}

#####################################
## VPC Module Information
## https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest
## Terraform module which creates VPC resources on AWS. In my case I am going to use existing VPC module
#####################################

#module "vpc" {
#  source  = "terraform-aws-modules/vpc/aws"
#  version = "2.77.0"
#
#  name                 = ""
#  cidr                 = ""
#  azs                  = data.aws_availability_zones.available.names
#  public_subnets       = ["", "", ""]
#  enable_dns_hostnames = true
#  enable_dns_support   = true
#}

#####################################
## Provides an RDS DB subnet group resource.In my case I am going to use existing Subnet Group. 
## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_subnet_group
#####################################


#resource "aws_db_subnet_group" "terraform-test" {
#  name       = "terraform-test"
#  subnet_ids = module.vpc.public_subnets
#
#  tags = {
#    Terraform = "true"
#    Environment = "perf"
#    Name = "yogeshpingle"
#  }
#}

#####################################
## Provides a security group resource.In my case I am going to use existing Security Group.
## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
#####################################

#resource "aws_security_group" "rds" {
#  name   = ""
#  vpc_id = module.vpc.vpc_id
#
#  ingress {
#    from_port   = 5432
#    to_port     = 5432
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#
#  egress {
#    from_port   = 5432
#    to_port     = 5432
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#
#  tags = {
#    Name = "education_rds"
#  }
#}

#####################################
## Provides an RDS DB parameter group resource .Documentation of the available parameters for various RDS engines can be found ## below :
##  https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_parameter_group
## Instead of "terraform-test" you can replace it with your parameter group.
#####################################

resource "aws_db_parameter_group" "terraform-test-mysql" {
  name   = "terraform-test-mysql"
  family = "mysql8.0"

  parameter {
    name  = "activate_all_roles_on_login"
    value = "0"
  }
   parameter {
    name  = "big_tables"
    value = "0"
  }
   parameter {
    name  = "bind_address"
    value = "*"
  }
   parameter {
    name  = "binlog_direct_non_transactional_updates"
    value = "0"
  }
   parameter {
    name  = "binlog_expire_logs_seconds"
    value = "2592000"
  }
   parameter {
    name  = "binlog_row_metadata"
    value = "MINIMAL"
  }
   parameter {
    name  = "collation_database"
    value = "utf8mb4"
  }
   parameter {
    name  = "cte_max_recursion_depth"
    value = "1000"
  }
   parameter {
    name  = "default_authentication_plugin"
    value = "mysql_native_password"
  }
   parameter {
    name  = "default_collation_for_utf8mb4"
    value = "utf8mb4_0900_ai_ci"
  }
   parameter {
    name  = "expire_logs_days"
    value = "2592000"
  }
   parameter {
    name  = "foreign_key_checks"
    value = "1"
  }
   parameter {
    name  = "gtid_mode"
    value = "0"
  }
   parameter {
    name  = "histogram_generation_max_mem_size"
    value = "20000000"
  }
   parameter {
    name  = "information_schema_stats_expiry"
    value = "86400"
  }
   parameter {
    name  = "innodb_data_file_path"
    value = "ibdata1:12M:autoextend"
  }
   parameter {
    name  = "innodb_doublewrite"
    value = "1"
  }
   parameter {
    name  = "innodb_doublewrite_batch_size"
    value = "16"
  }
   parameter {
    name  = "innodb_doublewrite_pages"
    value = "32"
  }
   parameter {
    name  = "innodb_force_recovery"
    value = "0"
  }
   parameter {
    name  = "innodb_log_files_in_group"
    value = "2"
  }
   parameter {
    name  = "innodb_log_spin_cpu_abs_lwm"
    value = "80"
  }
   parameter {
    name  = "innodb_log_spin_cpu_pct_hwm"
    value = "50"
  }
   parameter {
    name  = "innodb_log_wait_for_flush_spin_hwm"
    value = "400"
  }
   parameter {
    name  = "innodb_print_ddl_logs"
    value = "0"
  }
   parameter {
    name  = "innodb_redo_log_encrypt"
    value = "0"
  }
   parameter {
    name  = "innodb_segment_reserve_factor"
    value = "12.5"
  }
   parameter {
    name  = "innodb_stats_include_delete_marked"
    value = "0"
  }
   parameter {
    name  = "innodb_undo_log_encrypt"
    value = "0"
  }
   parameter {
    name  = "mysqlx_connect_timeout"
    value = "30"
  }
   parameter {
    name  = "mysqlx_max_connections"
    value = "100"
  }
   parameter {
    name  = "mysqlx_read_timeout"
    value = "28800"
  }
}

#####################################
## Instead of "Terraform Test" kindly put your RDS Instance name
## https://registry.terraform.io/modules/terraform-aws-modules/rds/aws/latest/examples/complete-postgres
## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance
#####################################

resource "aws_db_instance" "terraform-test-mysql" {
  identifier             = "terraform-test-mysql"
  instance_class         = "db.t3.micro"
  storage_type           = "gp2"
  allocated_storage      = 5
  max_allocated_storage = 100
  engine                 = "mysql"
  engine_version         = "8.0"
  username               = ""
  password               = ""
  db_subnet_group_name   = ""
  vpc_security_group_ids = [""]
  parameter_group_name   = aws_db_parameter_group.terraform-test-mysql.name
  skip_final_snapshot    = true
  performance_insights_enabled = true
  performance_insights_kms_key_id = ""
  performance_insights_retention_period = 7
  storage_encrypted = true
  kms_key_id = ""
  auto_minor_version_upgrade = false
  allow_major_version_upgrade = false
  backup_retention_period = 7
  backup_window = "03:28-03:58"
  copy_tags_to_snapshot = true
  deletion_protection = true
  enabled_cloudwatch_logs_exports = ["agent","error"]
}
