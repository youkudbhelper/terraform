#####################################
## Configure the AWS Provider
#####################################

#provider "aws" {
#  region = "ap-south-1"
#}

## Please go through this url "terraform data source" to understand how data sources work
## https://www.terraform.io/language/data-sources

data "aws_availability_zones" "available" {}

#####################################
## VPC Module Information
## https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest
## Terraform module which creates VPC resources on AWS. In my case I am going to use existing VPC module
#####################################

#module "vpc" {
#  source  = "terraform-aws-modules/vpc/aws"
#  version = "2.77.0"
#
#  name                 = ""
#  cidr                 = ""
#  azs                  = data.aws_availability_zones.available.names
#  public_subnets       = ["", "", ""]
#  enable_dns_hostnames = true
#  enable_dns_support   = true
#}

#####################################
## Provides an RDS DB subnet group resource.In my case I am going to use existing Subnet Group. 
## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_subnet_group
#####################################


#resource "aws_db_subnet_group" "terraform-test" {
#  name       = "terraform-test"
#  subnet_ids = module.vpc.public_subnets
#
#  tags = {
#    Terraform = "true"
#    Environment = "perf"
#    Name = "yogeshpingle"
#  }
#}

#####################################
## Provides a security group resource.In my case I am going to use existing Security Group.
## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
#####################################

#resource "aws_security_group" "rds" {
#  name   = ""
#  vpc_id = module.vpc.vpc_id
#
#  ingress {
#    from_port   = 5432
#    to_port     = 5432
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#
#  egress {
#    from_port   = 5432
#    to_port     = 5432
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#
#  tags = {
#    Name = "education_rds"
#  }
#}

#####################################
## Provides an RDS DB parameter group resource .Documentation of the available parameters for various RDS engines can be found ## below :
##  https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_parameter_group
## Instead of "terraform-test" you can replace it with your parameter group.
#####################################

resource "aws_db_parameter_group" "terraform-test" {
  name   = "terraform-test"
  family = "postgres12"

  parameter {
    name  = "log_connections"
    value = "1"
  }
}

#####################################
## Instead of "Terraform Test" kindly put your RDS Instance name
## https://registry.terraform.io/modules/terraform-aws-modules/rds/aws/latest/examples/complete-postgres
## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance
#####################################

resource "aws_db_instance" "terraform-test" {
  identifier             = "terraform-test"
  instance_class         = "db.t3.micro"
  storage_type           = "gp2"
  allocated_storage      = 5
  max_allocated_storage = 100
  engine                 = "postgres"
  engine_version         = "12.8"
  username               = ""
  password               = ""
  db_subnet_group_name   = ""
  vpc_security_group_ids = [""]
  parameter_group_name   = aws_db_parameter_group.terraform-test.name
  skip_final_snapshot    = true
  performance_insights_enabled = true
  performance_insights_kms_key_id = ""
  performance_insights_retention_period = 7
  storage_encrypted = true
  kms_key_id = ""
  auto_minor_version_upgrade = false
  allow_major_version_upgrade = false
  backup_retention_period = 7
  backup_window = "03:28-03:58"
  copy_tags_to_snapshot = true
  deletion_protection = true
  enabled_cloudwatch_logs_exports = ["postgresql","upgrade"]
}

/*
#####################################
Output values make information about your infrastructure available on the command line, and can expose information for other Terraform configurations to use. Output values are similar to return values in programming languages.

https://www.terraform.io/language/values/outputs
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance#attributes-reference
#####################################
*/

output "RDS_Endpoint"{
  value = aws_db_instance.terraform-test.endpoint
}
