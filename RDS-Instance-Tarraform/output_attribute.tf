/*
#####################################
We can use this below block to get an details of RDS Endpoint post RDS Instancen creation 
Just add this block in our origical code to get the RDS Endpoint Detaisl in below format :

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance#attributes-reference

If you do not specify any specify attributes such as "endpoint" then it will give all the attributes associated with this instance .

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

RDS_Endpoint = "terraform-test-yogesh.**********.ap-south-1.rds.amazonaws.com:5432"
#####################################
*/

output "RDS_Endpoint"{
  value = aws_db_instance.terraform-test.endpoint
}
